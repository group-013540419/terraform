terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.8.0"
    }
  }
}



provider "google" {
   credentials = "${file("project-g-424417-bf47a2716679.json")}"
   project     = "project-g-424417" 
   region      = "us-central1"
 }